import sh
import itertools
from bs4 import BeautifulSoup

def get_year_html(year):
    return str(sh.wget(
               "http://www.presidency.ucsb.edu/showelection.php?year=%d" % year,
               O="-"))

def get_year_table(year):
    html = get_year_html(year)
    b = BeautifulSoup(html)
    return b.find_all("table", "elections_states")[0]

def get_year_important_rows(year):
    table = get_year_table(year)
    for r in table.find_all("tr"):
        if r.td.a is not None:
            yield r

def atoi(s):
    return int(s.replace(',', ''))

def pctof(s):
    return float(s.replace('%', '')) / 100

def tuple_from_row(r):
    state_name = r.td.get_text() # Displayed text from first td
    cells = r.find_all("td")
    total_votes = atoi(cells[1].get_text())
    obama_votes = atoi(cells[2].get_text())
    obama_vote_percent = pctof(cells[3].get_text())
    romney_votes = atoi(cells[5].get_text())
    romney_vote_percent = pctof(cells[6].get_text())

    # Using a string comparison trick to select the
    # non-blank cell
    a, b = cells[4].get_text(), cells[7].get_text()
    ev = int(min(a, b))

    return (state_name, total_votes, obama_votes, obama_vote_percent,
            romney_votes, romney_vote_percent, ev)

def get_year_results(year):
    return map(tuple_from_row, get_year_important_rows(year))

all_years = itertools.chain([1789], xrange(1792, 2013, 4))

#TODO: Write a script to parse that shit and output CSV

if __name__ == "__main__":
    b = get_year_table(2012)