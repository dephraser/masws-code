# MASWS Assignment 1 - part 2

## Transforming the source data to RDF

Considered vocabularies:
http://www.w3.org/TR/owl-time/ - Designed for scheduling. Not actively
maintained or governed. Overkill?

## About the data:
### bg\_checks\_total:

Mandated by the Brady Handgun Violence Prevention Act of 1993 and launched by the FBI on November 30, 1998, NICS is used by Federal Firearms Licensees (FFLs) to instantly determine whether a prospective buyer is eligible to buy firearms or explosives.

Before ringing up the sale, cashiers call in a check to the FBI or to other designated agencies to ensure that each customer does not have a criminal record or isn’t otherwise ineligible to make a purchase.

More than 100 million such checks have been made in the last decade, leading to more than 700,000 denials.

### Reasons for denial
* Has been convicted in any court of a crime punishable by imprisonment for a term exceeding one year
* Is under indictment for a crime punishable by imprisonment for a term exceeding one year
* Is a fugitive from justice
* Is an unlawful user of or addicted to any controlled substance
* Has been adjudicated as a mental defective or committed to a mental institution
* Is illegally or unlawfully in the United States
* Has been discharged from the Armed Forces under dishonorable conditions
* Having been a citizen of the United States, has renounced U.S. citizenship
* Is subject to a court order that restrains the person from harassing, stalking, or threatening an intimate partner or child of such intimate partner
* Has been convicted in any court of a misdemeanor crime of domestic violence

### by\_state\_history.csv

### Total NICS Firearm Background Checks
http://www.fbi.gov/about-us/cjis/nics
November 30, 1998 - November 30, 2012

These statistics represent the number of firearm background checks initiated through the NICS. They do not represent the number of firearms sold. Based on varying state laws and purchase scenarios, a one-to-one correlation cannot be made between a firearm background check and a firearm sale.

Minimum length of 16 inches (40 cm) for rifle barrels and 18 inches (45 cm) for shotgun barrels

