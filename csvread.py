import csv

def get_bg_checks_total():
    with open('data-cleaned/bg_checks_total.csv', 'rb') as csvfile:
        totals_reader = csv.reader(csvfile, delimiter=',')
        totals_reader.next()
        for row in totals_reader:
            try:
                yield (row[0], row[-1])
            except IndexError:
                pass

def get_bg_checks_by_state_2012():
    with open('data-cleaned/by_state_2012.csv', 'rb') as csvfile:
        totals_reader = csv.reader(csvfile, delimiter=',')
        totals_reader.next()
        for row in totals_reader:
            try:
                yield (row[0], row[-3])
            except IndexError:
                pass

def get_bg_checks_by_state_history():
    with open('data-cleaned/by_state_history.csv', 'rb') as csvfile:
        totals_reader = csv.reader(csvfile, delimiter=',')
        totals_reader.next()
        for row in totals_reader:
            try:
                yield (row[0], row[-1])
            except IndexError:
                pass