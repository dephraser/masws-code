import rdflib
import csvread

def parse_bg_checks_total():
    g = rdflib.Graph()

    # All hail the Data Cube
    qb = rdflib.Namespace("http://purl.org/linked-data/cube#")
    g.bind("qb", qb)

    # Dublin Core, some document publishing terms
    dc = rdflib.Namespace("http://purl.org/dc/terms/")
    g.bind("dc", dc)

    # Some XML bollocks used for date
    g.bind("xsd", rdflib.namespace.XSD)

    # SDMX Dimensions - used for time periods
    sdmx_dimension = rdflib.Namespace("http://purl.org/linked-data/sdmx/2009/dimension#")
    g.bind("sdmx_dimension", sdmx_dimension)

    # We are creating a data set
    dataset = rdflib.URIRef(
        "http://vocab.inf.ed.ac.uk/dataset/fbi/background-checks")

    # The dataset is of type a qb dataset
    g.add((dataset, rdflib.RDF.type, qb['dataset']))

    # The dataset has an rdfs comment explaining what the dataset is
    g.add((dataset, rdflib.RDFS.comment,
        rdflib.Literal(
            "Total NCIS Federal Background Checks from 1998 - 2012",
            lang="en"
        )
    ))

    # Also a label
    g.add((dataset, rdflib.RDFS.label,
        rdflib.Literal(
            "Total background checks",
            lang="en"
        )
    ))

    # It was published by the FBI. We don't have a date.
    g.add((dataset, dc['publisher'],
        rdflib.URIRef("http://www.fbi.gov/about-us/cjis/nics")
    ))

    # Now let's define a structure for this data
    structure = rdflib.URIRef("http://vocab.inf.ed.ac.uk/dsd/fbi/background-checks")
    g.add((dataset, qb['structure'], structure))

    g.add((structure, rdflib.RDF.type, qb['DataStructureDefinition']))
    """
    How is the data structured?
    In this case it's month / number of checks
    """
    time_component = rdflib.BNode()
    g.add((
        time_component,
        qb['measure'],
        sdmx_dimension['refPeriod']
    ))
    g.add((
        time_component,
        qb['order'],
        rdflib.Literal(2)
    ))

    background_checks_component = rdflib.BNode()
    background_check = rdflib.URIRef('http://vocab.inf.ed.ac.uk/def/fbi/measure/background-check')

    g.add((
        background_checks_component,
        qb['measure'],
        background_check
    ))

    g.add((
        background_checks_component,
        qb['order'],
        rdflib.Literal(1)
    ))

    g.add((structure, rdflib.RDFS.label,
        rdflib.Literal("Total background checks DSD", lang="en")))

    g.add((structure, qb['component'], background_checks_component))
    g.add((structure, qb['component'], time_component))
    g.add((structure, qb['sliceKey'], rdflib.URIRef(
        'http://vocab.inf.ed.ac.uk/slice/fbi/background-checks-period'
    )))
     
    for row in csvread.get_bg_checks_total():
        # Now we add our "observations"
        observation = rdflib.BNode()

        g.add((observation, rdflib.RDF.type, qb['observation']))
        g.add((observation, qb['dataset'], dataset))
        g.add((observation, sdmx_dimension['refPeriod'], rdflib.Literal(row[0])))
        g.add((observation, background_check, rdflib.Literal(row[1])))

    print g.serialize(format="turtle")

if __name__ == "__main__":
    parse_bg_checks_total()

